FROM golang:1.13

EXPOSE 29050

COPY . .
RUN go build main.go

CMD ./main
