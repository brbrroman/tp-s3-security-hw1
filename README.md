Только HTTP прокси

Порт 29050

СБорка и запуск
```
docker build -t r.vekshin_proxy https/gitlab.com/brbrroman/tp-s3-security-hw1.git
docker run --rm -p 29050:29050 --name r.vekshin_proxy r.vekshin_proxy
```

Без сохранения данных, только вывод в консоль. Не пропускает https. Без повторных запросов.