package main

import (
	"crypto/tls"
	"io"
	"log"
	"net/http"
)

func copyHeader(res, src http.Header) {
	for key, values := range src {
		for _, value := range values {
			res.Add(key, value)
		}
	}
}

func main() {
	server := &http.Server{
		Addr: ":29050",
		Handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r.Method == http.MethodConnect {
				log.Println("TLS not supported")
			} else { // GET или POST
				resp, _ := http.DefaultTransport.RoundTrip(r)
				defer resp.Body.Close()
				copyHeader(w.Header(), resp.Header)
				w.WriteHeader(resp.StatusCode)
				io.Copy(w, resp.Body)
				log.Println(r.Method, r, w)
			}
		}),
		TLSNextProto: make(map[string]func(*http.Server, *tls.Conn, http.Handler)),
	}
	log.Fatal(server.ListenAndServe())
}
